import { ObjectId } from 'promised-mongo';

const { AuthenticationError, ForbiddenError } = require('apollo-server');

const { query } = require('nact');

const resource = 'post' || 'proposal';

module.exports = {

  Mutation: {
    changeQuiz: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args.id) {
        return await query(collectionItemActor, {
          type: 'quiz',
          search: { _id: args.id },
          input: args.input,
        }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'quiz', input: args.input }, global.actor_timeout);
    },
    changeQuizPage: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args.id) {
        delete args.input.quiz_id;
      } else {
        args.input.quiz_id = new ObjectId(args.input.quiz_id);
      }
      if (args.id) {
        return await query(collectionItemActor, {
          type: 'quiz_page',
          search: { _id: args.id },
          input: args.input,
        }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'quiz_page', input: args.input }, global.actor_timeout);
    },
    changeQuizPageAnswer: async (obj, args, ctx, info) => {
      const collectionItemActor = ctx.children.get('item');

      if (args.id) {
        delete args.input.page_id;
      } else {
        args.input.page_id = new ObjectId(args.input.page_id);
      }
      if (args.input.target_page_id) {
        args.input.target_page_id = new ObjectId(args.input.target_page_id);
      }
      if (args.id) {
        return await query(collectionItemActor, {
          type: 'quiz_page_answer',
          search: { _id: args.id },
          input: args.input,
        }, global.actor_timeout);
      }
      return await query(collectionItemActor, { type: 'quiz_page_answer', input: args.input }, global.actor_timeout);
    },
    deleteQuiz: async (obj, args, ctx, info) => await ctx.db.quiz.remove({ _id: new ObjectId(args.id) }),
    deleteQuizPage: async (obj, args, ctx, info) => await ctx.db.quiz_page.remove({ _id: new ObjectId(args.id) }),
    deleteQuizPageAnswer: async (obj, args, ctx, info) => await ctx.db.quiz_page_answer.remove({ _id: new ObjectId(args.id) }),

  },
  Query: {
    getQuiz: async (obj, args, ctx, info) => (await ctx.db.quiz.find({ _id: new ObjectId(args.id) }))[0],
    getQuizPage: async (obj, args, ctx, info) => (await ctx.db.quiz_page.find({ _id: new ObjectId(args.id) }))[0],
    getQuizPageAnswer: async (obj, args, ctx, info) => (await ctx.db.quiz_page_answer.find({ _id: new ObjectId(args.id) }))[0],
    getQuizes: async (obj, args, ctx, info) => await ctx.db.quiz.find(),
    getQuizPages: async (obj, args, ctx, info) => (await ctx.db.quiz_page.find()),
    getQuizPageAnswers: async (obj, args, ctx, info) => (await ctx.db.quiz_page_answer.find()),
  },
  Quiz: {
    start_page: async (obj, args, ctx, info) => {
      if (obj.start_page_id) {
        return (await ctx.db.quiz_page.find({ _id: new ObjectId(obj.start_page_id) }))[0];
      }
    },
    pages: async (obj, args, ctx, info) => await ctx.db.quiz_page.find({ quiz_id: new ObjectId(obj._id) }),
  },
  QuizPage: {
    answers: async (obj, args, ctx, info) => await ctx.db.quiz_page_answer.find({ page_id: new ObjectId(obj._id) }),
    quiz: async (obj, args, ctx, info) => (await ctx.db.quiz.find({ _id: new ObjectId(obj.quiz_id) }))[0],
  },
  QuizPageAnswer: {
    target_page: async (obj, args, ctx, info) => (await ctx.db.quiz_page.find({ _id: new ObjectId(obj.target_page_id) }))[0],
    page: async (obj, args, ctx, info) => (await ctx.db.quiz_page.find({ _id: new ObjectId(obj.page_id) }))[0],
  },
};
